import { createStore } from 'vuex';
import router from './../router';

export default createStore({
  state: {
    profile: null,
    todoList: null,
    authorization: null,
    addTodo: false,
  },
  mutations: {
    updatePayload(state, payload) {
      state[payload[0]] = payload[1]
    },
    userLogin(state, payload) {
      state.profile = payload
    },
    todosUpdate(state, payload) {
      state.todoList = payload;

    },
    updateTodosList(state, payload) {
      state.todoList = [...state.todoList, payload];
    }
  },
  actions: {
    showAddTodo({ commit }, payload) {
      commit('updatePayload', ['addTodo', payload])
    },
    addTodo({ commit }, payload) {
      commit('updateTodosList', payload)
    },
    async login({ commit, dispatch }, payload) {
      try {
        await fetch('https://jsonplaceholder.typicode.com/users')
          .then(response => response.json())
          .then(usersData => {
            for (let item = 0; item < usersData.length; item++) {
              if (usersData[item].username === payload.username) {
                commit('userLogin', usersData[item]);
                commit('updatePayload', ['authorization', true]);
                dispatch('getTodos');
                break;
              } else {
                commit('updatePayload', ['authorization', false]);
              }
            }
          });
      } catch (error) {
        console.error(`Error load users. ${error}`)
      }
    },
    async getTodos({ commit }) {
      try {
        await fetch('https://jsonplaceholder.typicode.com/todos')
          .then(response => response.json())
          .then(json => { commit('todosUpdate', json); })
          .then(() => { router.push('/home'); });
      } catch (error) {
        console.error(`Error load todos. ${error}`)
      }
    }
  },
})
